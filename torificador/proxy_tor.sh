#!/bin/sh
### hacemos flush de iptables (CUIDADO! si ya tenemos reglas configuradas!)
iptables -X
iptables -Z
iptables -F
iptables -t nat -F

### Seteamos algunas variables #Destinos que no queremos enrutar usando TOR (conexiones de red local privada)

_non_tor="192.168.0.0/16"
#UID con el que corre el servicio TOR (en Debian podemos obtenerlo usando "id debian-tor")
#_tor_uid="116"

#Un aporte de @jobregon para obtener el UID del usuario de tor automaticamente:
_tor_uid=$(id debian-tor | awk -F"=" '{print $2}' | awk -F "(" '{print $1}')

#Puerto de transporte TOR, configurado como dijimos arriba 
_trans_port="9040"

### set iptables *nat
iptables -t nat -A OUTPUT -m owner --uid-owner $_tor_uid -j RETURN
iptables -t nat -A OUTPUT -p udp --dport 53 -j REDIRECT --to-ports 53

#Permitimos salida normal a todos los hosts de $_non_tor
for _clearnet in $_non_tor 127.0.0.0/9 127.128.0.0/10; do
    iptables -t nat -A OUTPUT -d $_clearnet -j RETURN
done

#Redirigimos todo el resto del tráfico al puerto de transporte tor: 
iptables -t nat -A OUTPUT -p tcp --syn -j REDIRECT --to-ports $_trans_port

### Configuramos nuestro firewall en modo statefull para paquetes de conexiones establecidas: 
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT 

#Aceptamos todos los paquetes salientes a las redes excluidas de tor: 
for _clearnet in $_non_tor 127.0.0.0/8; do
    iptables -A OUTPUT -d $_clearnet -j ACCEPT 
done 

#Permitimos solo salida a paquetes tor para el resto de las redes: 
iptables -A OUTPUT -m owner --uid-owner $_tor_uid -j ACCEPT
iptables -A OUTPUT -j REJECT
