from flask import Flask
from flask import render_template #Permite importar templates

app = Flask(__name__) #Iniciar Flask

listaeventos = [
    {'id': 1, 'titulo': 'Titulo1', 'descripcion': 'Esto es un evento'},
    {'id': 2, 'titulo': 'Titulo2', 'descripcion': 'Esto es un evento'},
    {'id': 3, 'titulo': 'Titulo3', 'descripcion': 'Esto es un evento'},
    {'id': 4, 'titulo': 'Titulo4', 'descripcion': 'Esto es un evento'},
    {'id': 5, 'titulo': 'Titulo5', 'descripcion': 'Esto es un evento'},
    {'id': 6, 'titulo': 'Titulo6', 'descripcion': 'Esto es un evento'}
]

@app.route('/')
def index():
    titulo = "Título"
    subtitulo = "Subtítulo"
    texto = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere. Nulla sed consectetur ex. Nam sit amet porta arcu, non lacinia est. Cras a pretium nibh. Integer lectus dolor, convallis quis odio ac, rhoncus malesuada sem. Praesent finibus tellus vitae vestibulum aliquam. Morbi sit amet orci vel velit ornare tincidunt. Duis suscipit rutrum augue eu aliquam. In quis dictum nibh, vel elementum tellus. Mauris et libero vel nibh iaculis porttitor a sed quam. Curabitur at tellus neque. Etiam quis diam ligula."
    nombre = "María"
    lista = ["Elemento A","Elemento B","Elemento C","Elemento D", "Elemento E"]
    precio = 9.99
    imagen ="libro.jpg"
    return render_template('template1.html' , titulo = titulo, subtitulo = subtitulo, texto = texto, nombre = nombre, lista = lista, precio = precio, imagen=imagen) #Mostrar template y pasar variables

@app.route('/extension')
def extension():
    titulo = "Título"
    subtitulo = "Subtítulo"
    texto = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere. Nulla sed consectetur ex. Nam sit amet porta arcu, non lacinia est. Cras a pretium nibh. Integer lectus dolor, convallis quis odio ac, rhoncus malesuada sem. Praesent finibus tellus vitae vestibulum aliquam. Morbi sit amet orci vel velit ornare tincidunt. Duis suscipit rutrum augue eu aliquam. In quis dictum nibh, vel elementum tellus. Mauris et libero vel nibh iaculis porttitor a sed quam. Curabitur at tellus neque. Etiam quis diam ligula."
    nombre = "María"
    lista = ["Elemento A","Elemento B","Elemento C","Elemento D", "Elemento E"]
    precio = 9.99
    return render_template('template2.html' , titulo = titulo, subtitulo = subtitulo, texto = texto, nombre = nombre, lista = lista, precio = precio) #Mostrar template y pasar variables

@app.route('/macros')
def macros():
    productos = [
        {'titulo': 'Titulo 1', 'texto': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere.','precio':8.99},
        {'titulo': 'Titulo 2', 'texto': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere.','precio':10.99},
        {'titulo': 'Titulo 3', 'texto': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere.','precio':5.99},
        {'titulo': 'Titulo 4', 'texto': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere.','precio':5.99},
        {'titulo': 'Titulo 5', 'texto': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere.','precio':6.99},
        {'titulo': 'Titulo 6', 'texto': 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas vel metus nec mi ornare posuere.','precio':2.99},

    ]
    titulo = "Título"
    return render_template('template3.html' , productos = productos) #Mostrar template y pasar variables

if __name__ == '__main__': #Asegura que solo se ejectue el servidor cuando se ejecute el script directamente
    app.run(port = 8000, debug = True)
