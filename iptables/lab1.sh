#!/bin/bash

HOST="200.1.2.3"
ETH="eth0"

iptables -F
iptables -t nat -F
iptables -Z
iptables -P INPUT -j DROP
iptables -P OUTPUT -j DROP
iptables -P FORWARD -j DROP

# Habilitamos trafico local
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Habilitamos forwarding en las interfaces de red
echo 1 >/proc/sys/net/ipv4/ip_forward

# Habilitamos gestion de estados
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# --------------------------

# 1)
iptables -A OUTPUT -p tcp -m multiport --dports 80,443,53 -j ACCEPT
iptables -A OUTPUT -p udp --dport 53 -j ACCEPT

# 2)
iptables -A OUTPUT -p tcp -m multiport --dports 110,995 -j ACCEPT
iptables -A OUTPUT -p tcp -m multiport --dports 143,993 -j ACCEPT
iptables -A OUTPUT -p tcp -m multiport --dports 25,2525,465 -j ACCEPT

# 3)
iptables -A INPUT -p tcp --dport 22 -j ACCEPT

