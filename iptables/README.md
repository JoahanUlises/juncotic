# [Curso: Iptables desde cero](https://juncotic.com/cursos/iptables-siempre-quisiste-saber-firewall/)
## Lo que siempre quisiste saber sobre este firewall

### Material complementario - Resoluciones prácticas
Versiones preliminares alpha de las resoluciones prácticas de los laboratorios planteados en el curso.

Existen diversas maneras de resolver los enunciados, esta es solamente una propuesta factible.

Ante dudas, sugerencias, correcciones o reporte de errores, por favor comunicarse a [diego@juncotic.com](mailto:diego@juncotic.com)

Nuevas versiones se irán actualizando en este repositorio en línea.

### Gracias!

---

Desde ya quedas invitado a sumarte a la comunidad #JuncoTIC!
Síguenos en nuestras redes sociales y mantente informado sobre nuestros últimos lanzamientos y capacitaciones.

Te esperamos!

* Blog: http://juncotic.com/blog
* Cursos: http://juncotic.com/cursos
* Facebook: https://facebook.com/juncotic
* Twitter: https://twitter.com/juncotic
* Google+: https://plus.google.com/+Juncotic
* Youtube: https://www.youtube.com/c/juncotic?sub_confirmation=1
* Telegram: https://t.me/juncotic

* Info: [info@juncotic.com](mailto:info@juncotic.com)

Contamos con tu presencia en nuestras iniciativas!
¡Muchas gracias!
