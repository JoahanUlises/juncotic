#!/bin/bash

LAN1="192.168.10.0/24"
LAN2="192.168.20.0/24"

ETH_LAN1="eth0"
ETH_LAN2="eth1"
ETH_WAN="eth2"

PUB_IP="200.3.4.5"
INTRANET="192.168.20.10"

iptables -F
iptables -t nat -F
iptables -Z
iptables -P INPUT -j DROP
iptables -P OUTPUT -j DROP
iptables -P FORWARD -j DROP

# Habilitamos trafico local
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Habilitamos forwarding en las interfaces de red
echo 1 >/proc/sys/net/ipv4/ip_forward

# Habilitamos gestion de estados
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# Habilitamos el NAT saliente
iptables -t nat -A POSTROUTING -o $ETH_WAN -j SNAT --to-source $PUB_IP

# --------------------------



# 1)
iptables -A FORWARD -s $LAN1 -p tcp -m multiport --dports 80,443,53 -j ACCEPT
iptables -A FORWARD -s $LAN1 -p udp --dport 53 -j ACCEPT
iptables -A FORWARD -s $LAN2 -p tcp -m multiport --dports 80,443,53 -j ACCEPT
iptables -A FORWARD -s $LAN2 -p udp --dport 53 -j ACCEPT

# 2)
iptables -A FORWARD -s $LAN1 -p tcp -m multiport --dports 110,995 -j ACCEPT
iptables -A FORWARD -s $LAN1 -p tcp -m multiport --dports 143,993 -j ACCEPT
iptables -A FORWARD -s $LAN1 -p tcp -m multiport --dports 25,2525,465 -j ACCEPT

# 3)
iptables -A FORWARD -s $LAN1 -d $INTRANET -p tcp --dport 80 -j ACCEPT

# 4) por politicas por defecto

# 5)
iptables -A FORWARD -s $LAN1 -d $INTRANET -p tcp --dport 22 -j ACCEPT

# 6)
iptables -A INPUT -p tcp --dport 22 -j ACCEPT

