#!/bin/bash

LAN="192.168.10.0/24"
DMZ="192.168.20.0/24"

ETH_LAN="eth0"
ETH_WAN="eth1"

PUB_IP="200.3.4.5"
DMZ_WEB="192.168.20.10"
DMZ_MYSQL="192.168.20.20"

INET_CLIENT="11.22.33.44"

iptables -F
iptables -t nat -F
iptables -Z
iptables -P INPUT -j DROP
iptables -P OUTPUT -j DROP
iptables -P FORWARD -j DROP

# Habilitamos trafico local
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Habilitamos forwarding en las interfaces de red
echo 1 >/proc/sys/net/ipv4/ip_forward

# Habilitamos gestion de estados
iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

# Habilitamos el NAT saliente
iptables -t nat -A POSTROUTING -o $ETH_WAN -j SNAT --to-source $PUB_IP

# --------------------------



# 1)
iptables -A FORWARD -s $LAN -p tcp -m multiport --dports 80,443,53 -j ACCEPT
iptables -A FORWARD -s $LAN -p udp --dport 53 -j ACCEPT
iptables -A FORWARD -s $DMZ -p tcp -m multiport --dports 80,443,53 -j ACCEPT
iptables -A FORWARD -s $DMZ -p udp --dport 53 -j ACCEPT

# 2)
iptables -A FORWARD -s $LAN -d $DMZ_WEB -p tcp --dport 443 -j ACCEPT

# 3)
iptables -A INPUT -s $LAN -p tcp --dport 22 -j ACCEPT

# 4)
iptables -t -nat -A PREROUTING -p tcp --dport 443 -j DNAT --to-destination $DMZ_WEB:443
iptables -A FORWARD -d $DMZ_WEB -p tcp --dport 443 -j ACCEPT

# 5) Por politicas por defecto

# 6)
iptables -A FORWARD -s $DMZ_WEB -d $DMZ_MYSQL -p tcp --dport 3306 -j ACCEPT

# 7)
iptables -A FORWARD -s $INET_CLIENT -p tcp --dport 22 -j ACCEPT

