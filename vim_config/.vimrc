
" Optional:
set mouse=a
"set foldmethod=indent
set cursorline
set autoindent
set background=dark
set tabstop=2
"set columns=120
"set lines=50
"colorscheme desert
colorscheme railscasts2
set shiftwidth=2

"set list listchars=tab:❘-,trail:·,extends:»,precedes:«,nbsp:×
"set list listchars=tab:»-,trail:·,extends:»,precedes:«
set listchars=tab:\┆\ 
set list

set clipboard=unnamedplus

syntax on
set nu

if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

so ~/.vim/plugins.vim

set guifont=Monospace\ 9
nnoremap <C-Left> gt
nnoremap <C-Right> gT
" in NERDTree, to open-silently file in newtab with Enter, instead of default pressing "T" (same for not silently with Tab instead of t) 
let NERDTreeMapOpenInTab='<TAB>'
"let NERDTreeMapOpenInTabSilent='<ENTER>'


"map <F9> :w<CR>:!clear;python3 %; read<CR>"
autocmd FileType python map <buffer> <F9> :w<CR>:exec '!clear; python3' shellescape(@%, 1)<CR>
autocmd FileType python imap <buffer> <F9> <esc>:w<CR>:exec '!clear; python3' shellescape(@%, 1)<CR>
