# JuncoTIC - [juncotic.com](https://juncotic.com)
## Contenido complementario al blog y cursos impartidos.


---
Desde ya quedas invitado a sumarte a la comunidad #JuncoTIC!
Síguenos en nuestras redes sociales y mantente informado sobre nuestros últimos lanzamientos y capacitaciones.

Puedes encontrar nuestros cursos con el mayor descuento posible en http://juncotic.com/cursos

Te esperamos!

* Blog: http://juncotic.com/blog
* Youtube: https://www.youtube.com/juncotic?sub_confirmation=1
* Twitter: https://twitter.com/juncotic
* Telegram (canal): https://t.me/juncotic
* Telegram (grupo): https://t.me/+TpdovPR7rOEwMTkx
* Facebook: https://facebook.com/juncotic

* Info: [info@juncotic.com](mailto:info@juncotic.com)

Contamos con tu presencia en nuestras iniciativas!
¡Muchas gracias!

.
