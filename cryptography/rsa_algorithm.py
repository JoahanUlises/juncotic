#!/usr/bin/python
import sys, random
import click

"""
Algoritmo Euclidiano para buscar el máximo común divisor (GCD) entre dos números.
Rerurn: el GCD entre x e y.
"""
def computeGCD(x, y):
   while(y):
       x, y = y, x % y
   return abs(x)

@click.command()
@click.option("-p", help="p prime number.", prompt="p value: ", type = int, default=89)
@click.option("-q", help="q prime number.", prompt= "q value: ", type = int, default=23)
@click.option("-m", help="Plain text", prompt= "Plain Message: ", type = int, default=1234)
@click.option("-l", help="j limit", prompt= "j limit: ", type = int, default=10000)
def main(p, q, m, l):

    # Generación de la clave pública:

    # Calcular n:
    n= p*q
    print("n = ", n)

    # Calcular z:
    z = (p-1)*(q-1)
    print("z = ", z)

    # Buscar los valores k, números coprimos de z, y añadirlos a la lista k_es:
    k_es = []
    for i in range(z):
        if computeGCD(i,z) == 1:
            k_es.append(i)

    # Elegir un valor aleatorio de la lista k_es como valor de k:
    index = random.randint(0,len(k_es)-1)
    k = k_es[index]
    print("k = ", k)


    # Elegir un valor entero de j que verifique la congruencia lineal.
    # Los valores de j son infinitos.
    # Recorremos los primeros "l" enteros, verificando la ecuación.
    # Añadimos cada elemento encontrado a la lista j_es.
    j_es=[]
    for i in range(l):
        j=(1+i*z)/k
        if int(j) == j:
            j_es.append(j)

    # Elegir un valor aleatorio de la lista l_es como valor de j:
    index = random.randint(0,len(j_es)-1)
    print("index: ", index)
    j = int(j_es[index])
    print("j = ", j)


    # Encriptamos el mensaje M con la clave pública
    # Se cifra con la clave pública para obtener confidencialidad
    M = m
    C = (M**k) % n
    print("Encrypted: ", C)

    # Desencriptamos el mensaje C:
    C = int(C)
    M_new = (C**j) % n
    M_new = int(M_new)
    print("Decrypted: ", M_new)

    if M == M_new:
        print("El algoritmo funciona bien!")
    else:
        print("El mensaje M es demasiado grande para el tamaño de la clave.")

    # Encriptamos el mensaje M con la clave privada:
    # Puede usarse este mecanismo para obtener integridad y autenticidad
    M = m
    C = (M**j) % n
    print("Encrypted: ", C)

    # Desencriptamos el mensaje C:
    C = int(C)
    M_new = (C**k) % n
    M_new = int(M_new)
    print("Decrypted: ", M_new)

if __name__ == "__main__":
    main()
